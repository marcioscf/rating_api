export interface Rating {
    _id: string;
    url: string;
    rating: string;
    ip?: string;
    user_id?: string;
}
