"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const ratingSchema = new mongoose_1.Schema({
    url: {
        type: String,
        required: true,
    },
    rating: {
        type: String,
        required: true,
    },
    ip: {
        type: String,
        unique: true,
    },
    user_id: {
        type: String,
        unique: true,
    },
});
const ratingModel = (0, mongoose_1.model)('Rating', ratingSchema);
exports.default = ratingModel;
//# sourceMappingURL=ratings.model.js.map