import { Document } from 'mongoose';
import { Rating } from '../interfaces/ratings.interface';
declare const ratingModel: import("mongoose").Model<Rating & Document<any, any, any>, {}, {}>;
export default ratingModel;
