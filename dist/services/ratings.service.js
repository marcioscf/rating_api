"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const HttpException_1 = require("../exceptions/HttpException");
const ratings_model_1 = (0, tslib_1.__importDefault)(require("../models/ratings.model"));
const util_1 = require("../utils/util");
class RatingsService {
    constructor() {
        this.ratings = ratings_model_1.default;
        // public async findRatingByPage(page: string): Promise<Rating[]> {
        //   if (isEmpty(page)) throw new HttpException(400, 'Page not existent');
        //   const findRating: Rating = await this.ratings.find({ page: page });
        //   return findRating;
        // }
    }
    async createRating(ratingData) {
        if ((0, util_1.isEmpty)(ratingData))
            throw new HttpException_1.HttpException(400, "You're not ratingData");
        const createRatingData = await this.ratings.create(Object.assign({}, ratingData));
        return createRatingData;
    }
}
exports.default = RatingsService;
//# sourceMappingURL=ratings.service.js.map