/// <reference types="mongoose" />
import { CreateRatingDto } from '../dtos/rating.dto';
import { Rating } from '../interfaces/ratings.interface';
declare class RatingsService {
    ratings: import("mongoose").Model<Rating & import("mongoose").Document<any, any, any>, {}, {}>;
    createRating(ratingData: CreateRatingDto): Promise<Rating>;
}
export default RatingsService;
