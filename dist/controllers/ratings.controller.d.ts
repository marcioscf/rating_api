import { NextFunction, Request, Response } from 'express';
import ratingService from '../services/ratings.service';
declare class RatingsController {
    ratingService: ratingService;
    createRating: (req: Request, res: Response, next: NextFunction) => Promise<void>;
}
export default RatingsController;
