"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const ratings_service_1 = (0, tslib_1.__importDefault)(require("../services/ratings.service"));
class RatingsController {
    constructor() {
        this.ratingService = new ratings_service_1.default();
        // public getRatingByPage = async (req: Request, res: Response, next: NextFunction) => {
        //   try {
        //     const page: string = req.params.id;
        //     const findOneRatingData: Rating[] = await this.ratingService.findRatingByPage(page);
        //     res.status(200).json({ data: findOneRatingData, message: 'findOne' });
        //   } catch (error) {
        //     next(error);
        //   }
        // };
        this.createRating = async (req, res, next) => {
            try {
                const ratingData = req.body;
                ratingData.ip = req.header['x-forwarded-for'].split(',')[0] || req.socket.remoteAddress;
                console.log(ratingData);
                const createRatingData = await this.ratingService.createRating(ratingData);
                res.status(201).json({ data: createRatingData, message: 'created' });
            }
            catch (error) {
                next(error);
            }
        };
        // public getRatingByUser = async (req: Request, res: Response, next: NextFunction) => {
        //   try {
        //     const ratingId: string = req.params.id;
        //     const ratingData: CreateRatingDto = req.body;
        //     const updateRatingData: Rating = await this.ratingService.getRatingByUser(ratingId, ratingData);
        //     res.status(200).json({ data: updateRatingData, message: 'updated' });
        //   } catch (error) {
        //     next(error);
        //   }
        // };
        // public deleteRating = async (req: Request, res: Response, next: NextFunction) => {
        //   try {
        //     const ratingId: string = req.params.id;
        //     const deleteRatingData: Rating = await this.ratingService.deleteRating(ratingId);
        //     res.status(200).json({ data: deleteRatingData, message: 'deleted' });
        //   } catch (error) {
        //     next(error);
        //   }
        // };
    }
}
exports.default = RatingsController;
//# sourceMappingURL=ratings.controller.js.map