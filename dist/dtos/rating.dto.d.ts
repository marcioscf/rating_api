export declare class CreateRatingDto {
    url: string;
    rating: string;
    ip: string;
    user_id: string;
}
