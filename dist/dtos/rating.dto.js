"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreateRatingDto = void 0;
const tslib_1 = require("tslib");
const class_validator_1 = require("class-validator");
class CreateRatingDto {
}
(0, tslib_1.__decorate)([
    (0, class_validator_1.IsString)(),
    (0, tslib_1.__metadata)("design:type", String)
], CreateRatingDto.prototype, "url", void 0);
(0, tslib_1.__decorate)([
    (0, class_validator_1.IsString)(),
    (0, tslib_1.__metadata)("design:type", String)
], CreateRatingDto.prototype, "rating", void 0);
(0, tslib_1.__decorate)([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, tslib_1.__metadata)("design:type", String)
], CreateRatingDto.prototype, "ip", void 0);
(0, tslib_1.__decorate)([
    (0, class_validator_1.IsOptional)(),
    (0, class_validator_1.IsString)(),
    (0, tslib_1.__metadata)("design:type", String)
], CreateRatingDto.prototype, "user_id", void 0);
exports.CreateRatingDto = CreateRatingDto;
//# sourceMappingURL=rating.dto.js.map