"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const express_1 = require("express");
const ratings_controller_1 = (0, tslib_1.__importDefault)(require("../controllers/ratings.controller"));
const rating_dto_1 = require("../dtos/rating.dto");
const validation_middleware_1 = (0, tslib_1.__importDefault)(require("../middlewares/validation.middleware"));
class RatingsRoute {
    constructor() {
        this.path = '/ratings';
        this.router = (0, express_1.Router)();
        this.ratingsController = new ratings_controller_1.default();
        this.initializeRoutes();
    }
    initializeRoutes() {
        // this.router.get(`${this.path}`, this.ratingsController.getRatings);
        // this.router.get(`${this.path}/:id`, this.ratingsController.getRatingById);
        this.router.post(`${this.path}`, (0, validation_middleware_1.default)(rating_dto_1.CreateRatingDto, 'body'), this.ratingsController.createRating);
        //   this.router.put(`${this.path}/:id`, validationMiddleware(CreateRatingDto, 'body', true), this.ratingsController.updateRating);
        //   this.router.delete(`${this.path}/:id`, this.ratingsController.deleteRating);
    }
}
exports.default = RatingsRoute;
//# sourceMappingURL=rating.route.js.map