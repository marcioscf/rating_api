import RatingsController from '../controllers/ratings.controller';
import { Routes } from '../interfaces/routes.interface';
declare class RatingsRoute implements Routes {
    path: string;
    router: import("express-serve-static-core").Router;
    ratingsController: RatingsController;
    constructor();
    private initializeRoutes;
}
export default RatingsRoute;
