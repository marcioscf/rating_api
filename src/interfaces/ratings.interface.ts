export interface Rating {
  _id?: string;
  url: string;
  rating: number;
  ip?: string;
}
