import { NextFunction, Request, Response } from 'express';
import { CreateRatingDto } from '@/dtos/rating.dto';
import { Rating } from '@interfaces/ratings.interface';
import ratingService from '@services/ratings.service';

class RatingsController {
  private getIp = req => {
    return req.header['x-forwarded-for'] ? req.header['x-forwarded-for'].split(',')[0] : '' || req.headers['host'];
  };

  public ratingService = new ratingService();

  // public getRatingByPage = async (req: Request, res: Response, next: NextFunction) => {
  //   try {
  //     const page: string = req.params.id;
  //     const findOneRatingData: Rating[] = await this.ratingService.findRatingByPage(page);

  //     res.status(200).json({ data: findOneRatingData, message: 'findOne' });
  //   } catch (error) {
  //     next(error);
  //   }
  // };

  public createRating = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const ratingData: CreateRatingDto = req.body;
      ratingData.ip = this.getIp(req);
      const createRatingData: Rating = await this.ratingService.createRating(ratingData);

      res.status(201).json({ data: createRatingData, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public getRatingByPage = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const ratingId: string = req.params.id;
      const ip: string = this.getIp(req);
      const updateRatingData: any = await this.ratingService.findRatingByPage(ratingId, ip);

      res.status(200).json({ data: updateRatingData, message: 'aggregated' });
    } catch (error) {
      next(error);
    }
  };

  // public deleteRating = async (req: Request, res: Response, next: NextFunction) => {
  //   try {
  //     const ratingId: string = req.params.id;
  //     const deleteRatingData: Rating = await this.ratingService.deleteRating(ratingId);

  //     res.status(200).json({ data: deleteRatingData, message: 'deleted' });
  //   } catch (error) {
  //     next(error);
  //   }
  // };
}

export default RatingsController;
