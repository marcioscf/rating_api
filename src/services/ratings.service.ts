import bcrypt from 'bcrypt';
import { CreateRatingDto } from '@/dtos/rating.dto';
import { HttpException } from '@exceptions/HttpException';
import { Rating } from '@interfaces/ratings.interface';
import ratingModel from '@models/ratings.model';
import { isEmpty } from '@utils/util';

class RatingsService {
  public ratings = ratingModel;

  public async createRating(ratingData: CreateRatingDto): Promise<Rating> {
    if (isEmpty(ratingData)) throw new HttpException(400, "You're not ratingData");

    const createRatingData: Rating = await this.ratings.create({ ...ratingData });

    return createRatingData;
  }

  public async findRatingByPage(url: string, ip: string): Promise<Rating[]> {
    if (isEmpty(url)) throw new HttpException(400, 'Page not existent');
    if (url[0] != '/') {
      url = '/' + url;
    }
    const userRating: any = await this.ratings.aggregate([
      {
        $facet: {
          page_reviews: [
            {
              $match: {
                url: url,
              },
            },
            {
              $group: {
                _id: '$url',
                url: {
                  $push: '$url',
                },
                average_rating: {
                  $avg: '$rating',
                },
                total: {
                  $sum: 1,
                },
              },
            },
            {
              $project: {
                _id: 0,
                url: 0,
              },
            },
          ],
          user_review: [
            {
              $match: {
                url: url,
                ip: ip,
              },
            },
            {
              $project: {
                _id: 0,
                __v: 0,
                url: 0,
                ip: 0,
              },
            },
          ],
        },
      },
      {
        $unwind: {
          path: '$page_reviews',
        },
      },
      {
        $unwind: {
          path: '$user_review',
        },
      },
    ]);
    console.log(userRating);
    return userRating;
  }
}

export default RatingsService;
