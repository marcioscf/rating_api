import { model, Schema, Document } from 'mongoose';
import { Rating } from '@interfaces/ratings.interface';

const ratingSchema: Schema = new Schema({
  url: {
    type: String,
    required: true,
  },
  rating: {
    type: Number,
    required: true,
  },
  ip: {
    type: String,
    required: true,
  },
});

ratingSchema.index({ url: 1, ip: 1 }, { unique: true });

const ratingModel = model<Rating & Document>('Rating', ratingSchema);

export default ratingModel;
