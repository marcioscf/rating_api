import { IsString, IsOptional, IsNumber } from 'class-validator';

export class CreateRatingDto {
  @IsString()
  public url: string;

  @IsNumber()
  public rating: number;

  @IsOptional()
  @IsString()
  public ip: string;

  @IsOptional()
  @IsString()
  public user_id: string;
}
